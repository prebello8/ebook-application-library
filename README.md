# Ebook Application Library

An eBook is an electronic version of a traditional print book that can be read by using a personal computer or by using an eBook reader. (An eBook reader can be a software application for use on a computer, such as Microsoft's free Reader application